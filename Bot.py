"""Sviluppato da Davide Leone - leonedavide[at]protonmail.com - in Python 3.
    Distribuito sotto licenza GNU Affero GPL - Copyright 2018 Davide Leone
    Versione 1.3.2 MoonLight Uploader 06/12/18

    MoonBot is a Telegram Bot that offers a lot of different instuments.
    Copyright (C) 2018  Davide Leone

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>."""

from CONFIG import *
import techlib
import translator

def log(msg): #-- INIZIO DEL BOT --#
    global chat_id, mess_id, fields, style, lan, menù, arch
    fields = list(msg.keys()) #Valori che il messaggio porta con se
    if 'message' in fields: #L'utente ha premuto un bottone
        chat_id = msg['message']['chat']['id'] #Identificato dell'utente
        mess_id = msg['message']['message_id'] #Identificativo del messaggio
        tipo = 1 #Tipo di messaggio (1 indica il bottone, 0 il testo normale)
    else: #Si tratta di un testo normale
        chat_id = msg['chat']['id'] 
        mess_id= msg['message_id'] 
        tipo = 0
    arch = techlib.scarica(ssd,{}) #Scarica l'archivio
    
    if chat_id not in list(arch.keys()): #Utente non (ancora) presente in archivio
        if 'language_code' in list(msg['from'].keys()): #Nel caso non sia chiara la lingua dell'utente, viene impostata l'inglese
            lan = techlib.parole(msg['from']['language_code'])[0] #Estrae il valore relativo alla lingua dell'utente
            if 'it' in lan:
                lan = 'it'
            elif 'fr' in lan: 
                lan = 'fr'
            elif 'de' in lan:
                lan = 'de'
            else:
                lan = 'en'
        else:
            lan = 'en'
        #L'utente viene aggiunto all'archivio, con Markdown come formattazione e anteprima link attiva. In posts vengono raccolti i pastebin creati dall'utente.
        arch[chat_id] = {'menù':0,'lingua': lan,'formattazione':'Markdown','posts':{},'anteprima':True}
    #Qui il bot estrae tutto ciò che in archivio si ha sull'utente
    menù = arch[chat_id]['menù'] #Menù in uso; il valore è temporaneo e spiega al bot cosa sta facendo l'utente
    arch[chat_id]['menù'] = 0 #Viene resettato il valore menù
    lan = arch[chat_id]['lingua'] #Lingua dell'utente, en for english, it for italian, etc.
    style = arch[chat_id]['formattazione'] #Formattazione dei post; Mardkwon, HTML, None
    posts = arch[chat_id]['posts'] #Vengono qui raccolti i pastebin creati dall'utente per distinguerlo come creatore
    anteprima = arch[chat_id]['anteprima'] #Indica se l'anteprima web è attiva o meno

    if tipo == 1: #Questa sezione serve a trasformare i bottoni delle pagine di /mypastebin in un testo
        if techlib.parole(msg['data'])[0] == "/mypastebin":
            msg['text'] = msg['data']
            fields = list(msg.keys())
            tipo = 0
    
    try: #Il messaggio inizia ad essere processato; in caso di errore, questo viene riportato nel log
        if tipo == 1: #Si tratta di un bottone
            if msg['data'] == 'Conferma pastebin':
                arch[chat_id]['menù'] = 2
                tastiera = InlineKeyboardMarkup(inline_keyboard=([[InlineKeyboardButton(text='❌ ANNULLA',callback_data='Annulla pastebin')]]))
                bot.editMessageText((chat_id,mess_id),msg['message']['text'],reply_markup=tastiera)
            elif msg['data'] == 'Annulla pastebin':
                message = translator.testo(21,lan)
                bot.editMessageText((chat_id,mess_id),message,reply_markup=None)
            elif msg['data'] == "Annulla modifica":
                message = translator.testo(21,lan)
                bot.editMessageText((chat_id,mess_id),message,reply_markup=None)
            elif msg['data'] == 'Impostazioni lingua':
                tastiera = InlineKeyboardMarkup(inline_keyboard=(
                    [[InlineKeyboardButton(text=flag.flagize('Italiano :IT:'),callback_data='Imposta lingua-italiano'),
                    InlineKeyboardButton(text=flag.flagize('English :GB:'),callback_data='Imposta lingua-inglese')]]+
                    [[InlineKeyboardButton(text=flag.flagize('Français :FR:'),callback_data='Imposta lingua-francese'),
                      InlineKeyboardButton(text=flag.flagize('Deutsch :DE:'),callback_data='Imposta lingua-tedesco')]]
                    ))
                message = """EN: Here you can change the bot's language. Just select English for English.
\nIT: Da qui puoi cambiare le impostazioni della lingua. Clicca Italiano per l'italiano.
\nFR: Ici, vous pouvez changer la langue du bot. Il suffit de sélectionner Français pour le français.
\nDE: Davon können Sie die Spracheintellungen verändern. Klick "Deutsch" an, um Deutsch einzustellen."""
                bot.editMessageText((chat_id,mess_id),message,reply_markup=tastiera)
            elif msg['data'] == 'Impostazioni formattazione':
                tastiera = InlineKeyboardMarkup(inline_keyboard=(
                    [[InlineKeyboardButton(text='HTML',callback_data='Imposta formattazione-HTML')]]+
                    [[InlineKeyboardButton(text='Markdown',callback_data='Imposta formattazione-Markdown')]]+
                    [[InlineKeyboardButton(text='None',callback_data='Imposta formattazione-None')]]
                    ))
                message = translator.testo(35,lan)
                bot.editMessageText((chat_id,mess_id),message,reply_markup=tastiera)
            elif msg['data'] == 'Impostazioni anteprima':
                message = translator.testo(37,lan)
                tastiera = InlineKeyboardMarkup(inline_keyboard=(
                    [[InlineKeyboardButton(text=translator.testo(19,lan,0),callback_data='Imposta anteprima-FALSE')]]+
                    [[InlineKeyboardButton(text=translator.testo(19,lan,1),callback_data='Imposta anteprima-TRUE')]]
                    ))
                bot.editMessageText((chat_id,mess_id),message,reply_markup=tastiera)
            #Bottoni che attivano/disattivano una qualche impostazioni
            elif msg['data'] == 'Imposta lingua-italiano':
                arch[chat_id]['lingua'] = 'it'
                message = 'Lingua modificata con successo.'
                bot.editMessageText((chat_id,mess_id),message,reply_markup=None)
            elif msg['data'] == 'Imposta lingua-inglese':
                arch[chat_id]['lingua'] = 'en'
                message = 'Language successfully changed.'
                bot.editMessageText((chat_id,mess_id),message,reply_markup=None)
            elif msg['data'] == 'Imposta lingua-francese':
                arch[chat_id]['lingua'] = 'fr'
                message = "Changement de langue avec succès."
            elif msg['data'] == 'Imposta lingua-tedesco':
                arch[chat_id]['lingua'] = 'de'
                message = "Sprache erfolgreich geändert."
                bot.editMessageText((chat_id,mess_id),message,reply_markup=None)
            elif msg['data'] == 'Imposta formattazione-HTML':
                arch[chat_id]['formattazione'] = 'HTML'
                message = translator.testo(39,lan)
                bot.editMessageText((chat_id,mess_id),message,reply_markup=None)
            elif msg['data'] == 'Imposta formattazione-Markdown':
                arch[chat_id]['formattazione'] = 'Markdown'
                message = translator.testo(39,lan)
                bot.editMessageText((chat_id,mess_id),message,reply_markup=None)
            elif msg['data'] == 'Imposta formattazione-None':
                arch[chat_id]['formattazione'] = None
                message = translator.testo(39,lan)
                bot.editMessageText((chat_id,mess_id),message,reply_markup=None)
            elif msg['data'] == 'Imposta anteprima-TRUE':
                arch[chat_id]['anteprima'] = True
                message = translator.testo(41,lan,1)
                bot.editMessageText((chat_id,mess_id),message,reply_markup=None)
            elif msg['data'] == 'Imposta anteprima-FALSE':
                arch[chat_id]['anteprima'] = False
                message = translator.testo(41,lan,0)
                bot.editMessageText((chat_id,mess_id),message,reply_markup=None)
            else: #Potrebbe trattarsi del codice di un documento seguito da un'azione (es. pb-EF1UIHQk modifica)
                try: 
                    delta = techlib.parole(msg['data'])[1] #Recupera il valore dell'azione
                except:
                    delta = ''
                if delta == 'delete': #Cancella un pastebin
                    arc = techlib.scarica(raccolta,{})
                    del arc[techlib.parole(msg['data'])[0]]
                    message = translator.testo(43,lan)
                    bot.editMessageText((chat_id,mess_id),message,reply_markup=None)
                    techlib.carica(raccolta,arc)
                elif delta == 'modifica': #Modifica il testo di un pastebin
                    arch[chat_id]['menù'] = '3 '+techlib.parole(msg['data'])[0]
                    message = translator.testo(25,lan)
                    bot.editMessageReplyMarkup((chat_id,mess_id),reply_markup=None)
                    tastiera = InlineKeyboardMarkup(inline_keyboard=([[InlineKeyboardButton(text='❌ '+translator.testo(13,lan,1),callback_data='Annulla modifica')]]))
                    bot.sendMessage(chat_id,message,'Markdown',reply_markup=tastiera)
                    
                    
        elif menù == 1 and 'text' not in fields: #L'utente sta caricando un documento per averne il link relativo
            upload(msg)
        elif menù == 2 and 'text' in fields: #L'utente sta caricando un testo per averne il link relativo
            pastebin(msg)
        elif techlib.parole(menù)[0] == '3' and tipo == 0: #L'utente sta modificando il testo di un pastebin
            pastebin(msg,techlib.parole(menù)[1])
        elif 'text' in fields: #Si tratta di un testo semplice, come un comando; è possibile che l'utente voglia accedere ad un documento o ad un pastebin
            download(msg)
        
    except Exception as e: #C'è stato un errore
        techlib.scrivi(logfile,(time.ctime()+' '+str(e)+'\n')) #Log degli errori
        bot.sendMessage(chat_id,"Scusami, c'è stato un errore.\n\nSorry, an error occured.")
        
    techlib.carica(ssd,arch) #Aggiorna gli archivi

def upload(msg): #Fornisce il link relativo al documento (pastebin o file)
    global chat_id, mess_id, fields, lan, menù, arch
    dizionario = {'sticker':'st','document':'do','photo':'fo','audio':'ad','voice':'vo','video_note':'vn','video':'vd'} #Identificativo del tipo di documento
    for x in range(len(list(msg.keys()))): #Viene recuperato il valore che indica il tipo di documento
        if fields[x] in list(dizionario.keys()):
            tipo = fields[x]
    try: #Viene recuperato il valore code, che specifica di quale documento si tratti
        code = msg[tipo]['file_id']
    except:
        try:
            code = msg[tipo][0]['file_id']
        except:
            code = msg[tipo]['file_id'][0]
    link = 't.me/'+bot.getMe()['username']+'?start='+dizionario[tipo]+'-'+code #Deeplink che indica tipo di documento e documento, che l'utente potrà utilizzare per richiedere il file
    message = translator.testo(29,lan)+link
    bot.sendMessage(chat_id,message,None,True,True,mess_id)

def pastebin(msg,code=''): #Crea il pastebin, se code ha un valore, si riferisce al codice di un pastebin già esistente che va modificato
    global chat_id, mess_id, fields, style, lan, menù, arch
    arc = techlib.scarica(raccolta,{}) #Scarica gli archivi relativi ai pastebin
    pastebin = {'text':msg['text'],'formattazione':style,'creator':chat_id,'anteprima':arch[chat_id]['anteprima']}
    #Struttura del pastebin; le impostazioni del pastebin sono riprese da quelle del creatore. Viene caricato anche l'ID del creatore per distinguerlo.
    alfabeto = techlib.numeri_arabi+techlib.alfabeto_min+techlib.alfabeto_mai
    if code == '': #Genera un codice casuale da assegnare al nuovo pastebin
        while len(code)< 11 and code not in list(arc.keys()): #Si assicura il codice non sia già stato assegnato
            code = 'pb-' #Il codice del pastebin è contraddistinto dall'iniziale "pb-" (il codice delle foto, ad esempio, inizia invece con fo-)
            for x in range(8): #Il codice è di 8 simboli casuali
                code += alfabeto[random.randint(1,len(alfabeto))-1]            
    arc[code] = pastebin #Il pastebin viene aggiunto agli archivi 
    link = 't.me/'+bot.getMe()['username']+'?start='+code #Deeplink che indica il pastebin
     try: #Se la formattazione è corretta, salva il pastebin
        message = translator.testo(27,lan)+link
        test = bot.sendMessage(chat_id,msg['text'],style,True,True,mess_id)
        bot.editMessageText((test['chat']['id'],test['message_id']),message,'Markdown',True)
        techlib.carica(raccolta,arc) #Gli archivi relativi ai pastebin vengono aggiornati
    except:
        message = "Formattazione non corretta! Riprova."
        tastiera = InlineKeyboardMarkup(inline_keyboard=([[InlineKeyboardButton(text='❌ '+translator.testo(13,lan,1),callback_data='Annulla modifica')]]))
        bot.sendMessage(chat_id,message,'Markdown',True,True,mess_id,reply_markup=tastiera)
        arch = techlib.carica(ssd)
        arch['menù'] = menù
        techlib.carica(ssd,arch) #Aggiorna gli archivi

def download(msg):
    global chat_id, mess_id, fields, style, lan, menù, arch
    command = msg['text']
    
    if command.lower() == '/start': #Comando di start
        message = translator.testo(1,lan)
        bot.sendMessage(chat_id,message,'Markdown',None,True)
        
    elif command.lower() == '/upload': #Comando per l'upload
        message = translator.testo(3,lan)
        arch[chat_id]['menù'] = 1 #Il bot si prepara a ricevere un documento
        bot.sendMessage(chat_id,message,'Markdown',None,True)

        
    elif command.lower() == '/pastebin': #Comando per la creazione di un pastebin
        message = translator.testo(23,lan)+str(style)+'.'
        tastiera = InlineKeyboardMarkup(inline_keyboard=([[InlineKeyboardButton(text='✅ '+translator.testo(13,lan,0),callback_data='Conferma pastebin')]]))
        bot.sendMessage(chat_id,message,'Markdown',None,True,reply_markup=tastiera) #Viene richiesta conferma per la creazione per evitare ne vengano generati di inutili

    elif command.lower() == "/settings" or command.lower() == "/impostazioni": #Comando per le impostazioni
        message = translator.testo(31,lan)
        bo1 = translator.testo(33,lan,0) #Bottone "Formattazione"
        bo2 = translator.testo(33,lan,1) #Bottone "Anteprima link"
        tastiera = InlineKeyboardMarkup(inline_keyboard=(
        [[InlineKeyboardButton(text='Languages',callback_data='Impostazioni lingua'),
        InlineKeyboardButton(text=bo1,callback_data='Impostazioni formattazione')]]+
        [[InlineKeyboardButton(text=bo2,callback_data='Impostazioni anteprima')]]))
        bot.sendMessage(chat_id,message,'Markdown',None,True,reply_markup=tastiera)

    elif command.lower() == "/help": #Manuale del Bot
        message = translator.testo(5,lan)
        uno = translator.testo(15,lan,0) #Bottone "Sviluppatore"
        due = translator.testo(15,lan,1) #Bottone "Codice Sorgente"
        tastiera = InlineKeyboardMarkup(inline_keyboard=(
            [[InlineKeyboardButton(text=emojize(':bust_in_silhouette: '+uno),url=url_contatto)]]+
            [[InlineKeyboardButton(text=due,url=sourcelink)]]))
        bot.sendMessage(chat_id,message,'Markdown',None,True,reply_markup=tastiera)

    elif techlib.parole(command)[0] == '/mypastebin': #Lista dei pastebin creati dall'utente
        if len(techlib.parole(command)) > 1: #L'utente ha inserito un comando come "/mypastebin 2" per accedere ad una specifica pagina
            try:
                n = abs(int(techlib.parole(command)[1]))
            except:
                n = 1
        else:
            n = 1
        arc = techlib.scarica(raccolta,{}) #Viene scaricato l'archivio relativo ai pastebin
        val = [] #Valori che indicano i pastebin di proprietà dell'utente
        post = [] #Sono i pastebin recuperati che saranno illustati all'utente
        keys = list(arc.keys()) #Codici di tutti i pastebin creati
        keys = keys[::-1] #L'ordine è cronologico decrescente rispetto alla data di creazione (dal più nuovo al più vecchio)
        emoji = emojize(':link:') #Chain emoji, è messa prima di ogni link
        for x in range(len(keys)): #Analizza tutti i pastebin e seleziona quelli di cui l'utente è autore
            if arc[keys[x]]['creator'] == chat_id:
                val.append(keys[x])
        testo = translator.testo(7,lan) 
        if len(val) != 0: #L'utente ha creato dei pastebin
            for x in range(6): #Vengono recuperati i primi 6 pastebin
                try:
                    x += (n-1)*5 #Ad x viene aggiunto un valore pari a 5 volte il numero della pagina meno 1; serve a differenziare le pagine
                    post.append(val[x]) #Viene recuperato un pastebin
                except:
                    None

            if (len(post)) == 6: #Sono stati trovati 6 pastebin, mentre ogni pagina ne contiene 5
                del post[5] #Viene eliminato l'ultimo, e vengono impostati i bottoni per accedere alla pagina successiva
                if n > 1:
                     tastiera = InlineKeyboardMarkup(inline_keyboard=(
                         [[InlineKeyboardButton(text='▶️',callback_data='/mypastebin '+str(n+1))]]+
                         [[InlineKeyboardButton(text='◀',callback_data='/mypastebin '+str(n-1))]]
                         ))
                else:
                    tastiera = InlineKeyboardMarkup(inline_keyboard=([[InlineKeyboardButton(text='▶️',callback_data='/mypastebin '+str(n+1))]]))
            elif n > 1:
                tastiera = InlineKeyboardMarkup(inline_keyboard=([[InlineKeyboardButton(text='◀',callback_data='/mypastebin '+str(n-1))]]))
            else: #Si è alla prima pagina, e sono stati trovati tutti i pastebin; non servono altre pagine.
                tastiera = None
            if len(post) == 0: #Non sono stati trovati pastebin in quella pagina
                testo = translator.testo(9,lan)
            else:
                testo = '' #Testo del messaggio finale
            for x in range(len(post)):
                testo+= emoji+' t.me/'+bot.getMe()['username']+'?start='+post[x]+'\n['+str(x+1+5*(n-1))+'] ' #10
                anteprima = arc[post[x]]['text'] #Assieme ad ogni link, viene fornita una anteprima di 140 caratteri del testo del pastebin
                anteprima = techlib.sostituisci(anteprima,'\n',' ') #Chiaramente, i daccapo non vengono presi in considerazione
                if len(anteprima)>140: #Taglia l'anteprima a 140 caratteri
                    anteprima = techlib.estrai(anteprima,137) #Toglie gli ultimi 3 e aggiunge altrettanti punti di sospensione
                    testo+= anteprima+'...\n\n'
                else: #I daccapo creano una distanza fra un'anteprima e l'altra
                    testo+= anteprima+'\n\n'
        if 'data' in list(msg.keys()): #Si tratta di un bottone; il testo viene modificato
            bot.editMessageText((chat_id,mess_id),testo,None,True,tastiera)
        else:
            bot.sendMessage(chat_id,testo,None,True,reply_markup=tastiera)
        
    elif techlib.parole(command)[0] == '/start' and len(command) > 10:
        code = techlib.parole(command)[1] #Il codice corrisponde al codice del documento (pastebin o file) nella forma TIPO DI DOCUMENTO - ID DEL DOCUMENTO (es. pb-INvkkYDg). 
        delta = techlib.estrai(code,2) #Estrae il codice che indica il tipo di file
        dizionario = {'ad':'audio','do':'document','fo':'photo','st':'sticker','vd':'video','vn':'video_note','vo':'voice'}
        #In base al tipo di file, cerca di recuperare il documento e quindi inviarlo all'utente
        if delta in list(dizionario.keys()):
            tipo = dizionario[delta]
            file_id = techlib.estrai(code,-len(code)+3)
            if tipo == "sticker":
                bot.sendSticker(chat_id,file_id)
            elif tipo == "document":
                bot.sendDocument(chat_id,file_id)
            elif tipo == "photo":
                try:
                    bot.sendMediaGroup(chat_id,file_id)
                except:
                    bot.sendPhoto(chat_id,file_id)
            elif tipo == "audio":
                bot.sendAudio(chat_id,file_id)
            elif tipo == "voice":
                bot.sendVoice(chat_id,file_id)
            elif tipo == "video_note":
                bot.sendVideoNote(chat_id,file_id)
            elif tipo == "video":
                bot.sendVideo(chat_id,file_id)
        elif delta == 'pb': #Si tratta di un pastebin
            arc = techlib.scarica(raccolta,{}) #Viene scaricato l'archivio relativo ai pastebin
            if code in list(arc.keys()):
                tastiera = None
                if chat_id == arc[code]['creator']: #L'utente è il creatore del post
                    tas = '❌ '+translator.testo(17,lan,0) #Bottone "ELIMINA"
                    ta2 = "✍️ "+translator.testo(17,lan,1) #Bottone "MODIFICA"
                    tastiera = InlineKeyboardMarkup(inline_keyboard=(
                        [[InlineKeyboardButton(text=tas,callback_data=(code+' delete'))]]+
                        [[InlineKeyboardButton(text=ta2,callback_data=(code+' modifica'))]]))
                bot.sendMessage(chat_id,arc[code]['text'],arc[code]['formattazione'],arc[code]['anteprima'],False,None,tastiera)
            else: #Il pastebin non è stato trovato
                message = translator.testo(11,lan)
                bot.sendMessage(chat_id,message,'Markdown',None,True,mess_id)

MessageLoop(bot,log).run_as_thread() #Riceve i messaggi
while 1: #Tiene attivo il bot
    time.sleep(5)
