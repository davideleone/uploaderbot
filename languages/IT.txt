---- 1. Messaggio di start
Benvenuto!\nGrazie a questo bot puoi condividere, *all'interno di Telegram*, file e testi in modo anonimo tramite un semplice link.\nUsa /upload per i file e /pastebin per i testi.
---- 3. Messaggio di upload
Mandami ora il file di cui vuoi ottenere il link.\nPuoi inviare video, documenti, foto, gif e molto altro...
---- 5. Manuale
*Manuale del bot*\nGrazie a questo bot potrete ottenere *link per condividere, su Telegram, testi e file.*\n/upload - Link per condividere foto, video, gif, sticker, documenti  etc.\n/pastebin - Link per condividere il testo di un messaggio.\n/mypastebin - Registro dei pastebin da te creati.\n/impostazioni - Impostazioni su lingua, stile di formattazione ed anteprima link.\n\nPuoi cancellare i tuoi pastebin usando lo stesso link: se sei il creatore apparir� un tasto per eliminarlo. I file caricati con /upload non possono essere eliminati.\nIl bot conserva unicamente i pastebin, archiviandone testo, impostazioni ed id del creatore.
---- 7. Errore 1
Non hai ancora creato dei pastebin!
---- 9. Errore 2
Stai cercando di accedere ad una pagina che non esiste.\nNon hai creato cos� tanti pastebin!
---- 11. Errore 3
Non ho trovato il testo. Forse � stato cancellato.
---- 13. Bottone conferma
CONFERMA|ANNULLA
--- 15. Bottoni manuale
Sviluppatore|Codice sorgente
--- 17. Bottoni pastebin
ELIMINA|MODIFICA
--- 19. Bottoni impostazioni
Abilitata|Disabilitata
---- 21. Operazione annullata
Operazione correttamente annullata.
---- 23. Testo conferma pastebin
Prima di procedere, clicca sul bottone qui sotto per confermare. Mandami poi il testo di cui vuoi ottenere un link condivisibile.\nChiunque cliccher� sul link potr� leggere il testo, senza che gli sia rivelato l'autore.\nPotrai sempre cancellare il testo, semplicemente cliccando tu stesso il link.\nRicorda che il testo sar� formattato in 
---- 25. Modifica pastebin
Inviami ora il nuovo messaggio.\nIl nuovo messaggio andr� a *rimpiazzare* il precedente, ed il link sar� ovviamente lo stesso.\n*Ricorda che le impostazioni del post* (es. formattazione) *saranno sovrascritte.*
---- 27. Link
Puoi condividere il testo con questo link: 
---- 29. Link/2
Puoi condividere il file usando questo link: 
---- 31. Impostazioni
Benvenuto nelle impostazioni del bot.\nPuoi usare i bottoni qui sotto per cambiare la lingua del bot o lo stile di formattazione - Markdown, HTML, o nessuno dei due.
--- 33. Impostazioni-varie
Formattazione|Anteprima link 
---- 35. Impostazioni formattazione
Da qui puoi modificare le impostazioni di formattazione per i tuoi futuri pastebin.\nPuoi utilizzare l'HTML, il Markdown o nessuno dei due.
---- 37. Impostazioni link
Da qui puoi modificare le impostazioni relative all'anteprima link dei tuoi futuri pastebin.
---- 39. Successo formattazione
Stile di formattazione modificato con successo.
--- 41. Successo anteprima link
Anteprima web abilitata con successo.|Anteprima web disabilitata con successo.
---- 43. Eliminato
Messaggio correttamente eliminato dagli archivi del bot.
