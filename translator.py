""" Programmato da Davide Leone in Python 3
    Per contatti: leonedavide[at]protonmail.com

    File di sistema per MoonLigh Uploader 1.3.1
    Copyright (C) 2018  Davide Leone

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>."""

#Import delle varie librerie
import techlib
from CONFIG import *

def testo(line,lingua='en',k=0):
    filename = folder_translation+(lingua.upper())+'.txt'
    try:
        file = open(filename,'r')
    except:
        file = open(folder_translation+'EN.txt','r')
    text = file.readlines()
    line = text[line]
    if '|' in line:
        line = techlib.parole(line,'|')[k]
    if line[-1] == '\n':
        line = techlib.elimina(line,-1)
    line = line.replace("\\n","\n")
    return line
    
