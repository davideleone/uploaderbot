# -*- coding: utf-8 -*-
""" Programmato da Davide Leone in Python 3
    Per contatti: leonedavide[at]protonmail.com
    File di configurazione per MoonLigh Uploader 1.3.2
    Copyright (C) 2018  Davide Leone
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.
    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>."""

#Import delle varie librerie
import time
import string
import random
import telepot
from telepot.loop import MessageLoop
import pickle
from emoji import emojize
import flag
from telepot.namedtuple import InlineKeyboardMarkup, InlineKeyboardButton, ReplyKeyboardMarkup, KeyboardButton, ReplyKeyboardRemove

bot = telepot.Bot('') #Bot's token
pos = "" #Files's folder position in the computer
ssd = pos+'database.save' #Database for users and their settings
raccolta = pos+'pastebin.save' #Database for pastebins
logfile = pos+'error.log' #Error log 
sourcelink = "https://gitlab.com/davideleone/uploaderbot" #Source code link
folder_translation = pos+'languages/' #Folder with all the languages supported
url_contatto = 't.me/DavideLeone' #Telegram link to contact the manteiner 