# uploaderbot

This is a Telegram Bot that allows you to obtain, in a privacy-friendly way, a link with you can share files or text (formated as you want) all over the platform.

Most notable functions: 
- You can get a link with you can share files
- You can create pastebin inside Telegram, formatted as you want
- You can consult a list of your pastebins and modify them 
- It's a multilanguage bot (currently, it supports english, french, italian and german)
- It's privacy-friendly.

A special thanks to Giovanni Zaccaria for the german translation. 

This bot was created and is actually manteined by Davide Leone, if you wnat to contact me please write to davideleone [at] protonmail.com